# Cargalogic BOT Whatsapp

## Setup
Ejecutar el servidor de selenium con el comandos
```bash
java -jar selenium-server-standalone-3.141.59.jar
```

Ejecutar los siguientes comandos al instalar las librerias de WebWhatsapp-Wrapper

```bash
pip3 install --upgrade pip
pip uninstall python-magic
pip install python-magic-bin==0.4.14
```
