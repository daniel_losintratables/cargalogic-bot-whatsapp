import os
import requests
import json
import sys
import time
from webwhatsapi import WhatsAPIDriver
from flask import Flask, request,  send_file, abort, g
from celery import Celery


def make_celery(app):
    celery = Celery(
        app.import_name,
        backend=app.config['CELERY_RESULT_BACKEND'],
        broker=app.config['CELERY_BROKER_URL']
    )
    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    return celery


flask_app = Flask(__name__)
print("............ " + str(flask_app.name))
flask_app.config.update(
    CELERY_BROKER_URL='redis://localhost:6379',
    CELERY_RESULT_BACKEND='redis://localhost:6379'
)
celery = make_celery(flask_app)


# @celery.task()
def add_together(a, b):
    return a + b


@celery.task()
def run_whatsapp():
    # print("Environment", os.environ)
    try:
        print(os.environ["SELENIUM"])
    except KeyError:
        print("Please set the environment variable SELENIUM to Selenium URL")
        sys.exit(1)
    host = 'localhost'
    port = '4444'
    url = 'http://' + host + ':' + port + '/wd/hub'
    print(url)
    global driver
    driver = WhatsAPIDriver(client="chrome", command_executor=url,
                            profile="/home/ubuntu/.config/google-chrome")
    print("Waiting for QR")
    driver.wait_for_login()
    print("Bot started")
    # sendMessage(driver, test_nums)
    driver.subscribe_new_messages(NewMessageObserver(driver))
    print("Waiting for new messages...")
    """ Locks the main thread while the subscription in running """
    while True:
        time.sleep(60)


class NewMessageObserver:
    def __init__(self, driver):
        self.driver = driver

    def on_message_received(self, new_messages):
        for message in new_messages:
            phone = message.sender.id.split("@")
            if message.type == "chat":
                print(
                    "New message '{}' received from number {}".format(
                        message.content, message.sender.id
                    )
                )
                record = {
                    "message": message.content,
                    "location": "N/A",
                    "date_time": message.timestamp.strftime("%Y-%m-%d %H:%M:%S"),
                    "notes": "nota de prueba",
                    "phone": phone[0],
                    "type": message.type
                }
                print(record)
                url = "https://comp1.gumda.app/api/method/cargalogic.cargalogic.doctype.journey.journey.saveTracking"

                headers = {
                    'Authorization': 'token e32f4c593a83b44:b70d5e02600a0a4',
                    'Content-Type': 'application/json',
                    'Cookie': 'sid=Guest; system_user=yes; full_name=Guest; user_id=Guest; user_image='
                }

                response = requests.request("POST", url, headers=headers, data=json.dumps(record))
                print(response.text)
                # self.driver.send_message_to_id(message.sender.id, msg)
                # return message.content
            else:
                print(
                    "New message of type '{}' received from number {}".format(
                        message.type, message.sender.id
                    )
                )
                record = {
                    "message": 1,
                    "location": "(" + str(message.latitude) + "/" + str(message.longitude) + ")",
                    "lat": message.latitude,
                    "long": message.longitude,
                    "date_time": message.timestamp.strftime("%Y-%m-%d %H:%M:%S"),
                    "notes": "nota de prueba",
                    "phone": phone[0],
                    "type": message.type
                }
                print(record)
                url = "https://comp1.gumda.app/api/method/cargalogic.cargalogic.doctype.journey.journey.saveTracking"

                headers = {
                    'Authorization': 'token e32f4c593a83b44:b70d5e02600a0a4',
                    'Content-Type': 'application/json',
                    'Cookie': 'sid=Guest; system_user=yes; full_name=Guest; user_id=Guest; user_image='
                }

                response = requests.request("POST", url, headers=headers, data=json.dumps(record))
                print(response.text)
                # return message.type


def saveMessageRegister(answer):
    print("saveMessageRegister")
    # print(dir(answer))
    if answer.latitude:
        location = "(" + answer.latitude + "," + answer.longitude + ")"
    else:
        location = "N/A"
    record = {
        "message": answer.content,
        "location": location,
        "date_time": answer.timestamp,
        "notes": "nota de prueba",
        "phone": "+593960113142"
    }
    url = "http:///comp1.gumda.app/api/method/cargalogic.cargalogic.doctype.services.services.saveTracking"

    headers = {
        'Authorization': 'token e32f4c593a83b44:b70d5e02600a0a4',
        'Content-Type': 'application/json',
        'Cookie': 'sid=Guest; system_user=yes; full_name=Guest; user_id=Guest; user_image='
    }

    response = requests.request("POST", url, headers=headers, data=record)

    print(response.text)


if __name__ == "__main__":
    flask_app.run()
    # print("es main")
    # result = run_whatsapp.delay()
    # result.wait()


@flask_app.route('/sendMessage', methods=["POST"])
def send_message():
    data = request.get_json()
    print(data)
    try:
        for i in data["phone_id"]:
            print(i["no"])
            driver.send_message_to_id(i["no"] + "@c.us", data["content"])
        return {"status": 200}
    except Exception as e:
        return json.dump({"error": e})


@flask_app.route('/test')
def test():
    run_whatsapp().delay()
    return "bot started"

